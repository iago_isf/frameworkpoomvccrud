<?php
class File 
{
  private $file;
  private $types;
  
  public function __construct($file, $types)
  {
    $this->file = $file;
    $this->types = $types;
  }

  public function checkFileErrors()
  {
    if ($this->file['image']['error'] !== UPLOAD_ERR_OK) { 
    // $_FILES es un array asociativo de archivos subidos con el método post || UPLOAD_ERR_OK comprueba que no haya errores
    
      switch ($this->file['image']['error']) {
        case UPLOAD_ERR_INI_SIZE: // Ambos códigos verifican que el tamaño del archivo sea menor al especificado
        case UPLOAD_ERR_FORM_SIZE: // INI comprueba el archivo del servidor y FORM el del formulario
          throw new FileException('El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize'));
          // ini_get devuelve el valor que tiene una variable en la configuración
          break;
        case UPLOAD_ERR_PARTIAL: // El archivo no se ha subido completamente al servidor
          throw new FileException('No se ha podido subir el fichero completo.');
          break;
        default:
          throw new FileException('Error al subir el fichero.');
          break;
      }
    }
  }

  public function saveUploadFile($url)
  {
    if (in_array($this->file['image']['type'], $this->types) === false) { // in_array busca en el string especificado la cadena que queremos
      throw new FileException('Tipo de fichero no soportado.');

    } else if (is_uploaded_file($this->file['image']['tmp_name']) === false) { // is_uploaded_file verifica que el archivo haya sido subido por POST
      throw new FileException('El archivo no se ha subido mediante un formulario.');

    } else if (move_uploaded_file($this->file['image']['tmp_name'], $url . $this->file['image']['name']) === false) { 
      // move_uploaded_file asegura que el archivo esté subido y lo mueve del directorio temporal a la carpeta en la que se guardará
      throw new FileException('Ocurrió algún error al subir el fichero. No pudo guardarse.');
    }
  }
}
?>