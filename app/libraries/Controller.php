<?php
class Controller
{

  public function __construct()
  {

  }
  
  public function model ($modelo) 
  {
    if (file_exists('../app/models/' . $modelo . '.php')) { 
      require_once '../app/models/' . $modelo . '.php';
      return new $modelo();

    } else {
      die('No se ha encontrado el modelo');
    }

    
  }

  public function view ($vista, $data=[]) 
  {
    if (file_exists('../app/views/' . $vista . '.php')) { 
      require_once '../app/views/' . $vista . '.php';

    } else {
      die('No se ha encontrado la vista');
    }
  }
}
?>