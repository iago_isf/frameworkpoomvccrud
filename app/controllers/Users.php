<?php
use \Tamtamchik\SimpleFlash\Flash;

class Users extends Controller
{
  private $user;

  public function __construct()
  {
    $this->user = $this->model('User');  
  }

  public function register()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Si existen parámetros enviados por POST
      $args = [
        'name' => FILTER_SANITIZE_STRING,
        'email' => FILTER_SANITIZE_EMAIL,
        'password' => FILTER_SANITIZE_STRING,
        'confirm_password' => FILTER_SANITIZE_STRING,
      ];

      $data = filter_input_array(INPUT_POST, $args, true); // Filtrar todos los parámetros que se envian por POST
      $data['name'] = trim($data['name'], ' '); // Limpiar los espacios blancos
      $data['email'] = trim($data['email'], ' ');
      $data['password'] = trim($data['password'], ' ');
      $data['confirm_password'] = trim($data['confirm_password'], ' ');

      // Validar el nombre
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'is-invalid';
        $this->view('users/register', $data);
      }

      // Validar el email
      else if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL) || $this->user->findUserByEmail($data['email'])){
        $data['email_err'] = 'is-invalid';
        $this->view('users/register', $data);
      }

      // Validar la contraseña
      else if(!isset($data['password']) || strlen($data['password']) < 6){
        $data['password_err'] = 'is-invalid';
        $this->view('users/register', $data);
      }

      // Validar la confirmación de contraseña
      else if(!isset($data['confirm_password']) || $data['confirm_password'] != $data['password']){
        $data['confirm_password_err'] = 'is-invalid';
        $this->view('users/register', $data);

      }

      // Si todo está bien se envia el registro
      else {
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        if($this->user->register($data)){
          Flash::message('Ya estás registrado y puedes iniciar sesión.', 'info');
          
          $this->view('users/login');
        
        } else {
          echo 'Registro no realizado';
        }
      }

    } else { // En caso de que no haya nada enviado por POST
      $this->view('users/register');

    }
  }

  public function login()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Si existen parámetros enviados por POST
      $args = [
        'email' => FILTER_SANITIZE_EMAIL,
        'password' => FILTER_SANITIZE_STRING,
      ];

      $data = filter_input_array(INPUT_POST, $args, true); // Filtrar todos los parámetros que se envian por POST
      $data['email'] = trim($data['email'], ' '); // Limpiar los espacios blancos
      $data['password'] = trim($data['password'], ' ');

      // Validar el email
      if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL) || !$this->user->findUserByEmail($data['email'])){
        $data['email_err'] = 'is-invalid';
        $this->view('users/login', $data);
      }

      // Validar la contraseña
      else if(!isset($data['password']) || strlen($data['password']) < 6){
        $data['password_err'] = 'is-invalid';
        $this->view('users/login', $data);
      }

      // Si todo está bien se envia el registro
      else {
        $user = $this->user->login($data);

        if ($user) {
          $this->createUserSession($user);

        } else {
          $data['password_err'] = 'is-invalid';
          $this->view('users/login', $data);
        }
      }

    } else { // En caso de que no haya nada enviado por POST
      $this->view('users/login');

    }
  }

  public function logout()
  {
    unset($_SESSION['user_id']);
    unset($_SESSION['user_name']);
    unset($_SESSION['user_email']);

    session_destroy();

    urlHelper('users/login');
  }

  private function createUserSession($user)
  {
    $_SESSION['user_id'] = $user->id;
    $_SESSION['user_name'] = $user->name;
    $_SESSION['user_email'] = $user->email;

    urlHelper('posts/index');
  }
}
?>