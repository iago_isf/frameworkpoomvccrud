<?php
use \Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{
  private $post;

  function __construct()
  {
    if(!isLoggedIn()){
      urlHelper('users/login');
    }

    $this->post = $this->model('Post');
  }

  function index()
  {
    $data = $this->post->getPosts();
    $this->view('posts/index', $data);
  }

  function show($id)
  {
    $data = $this->post->getPostByID($id);
    
    $this->view('posts/show', $data);
  }

  function add()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $args = [
        'title' => FILTER_SANITIZE_STRING,
        'body' => FILTER_SANITIZE_EMAIL,
      ];

      $data = filter_input_array(INPUT_POST, $args, true);
      $data['image'] = !empty($_FILES) ? $_FILES['image']['name'] : '';

      if(!isset($data['title']) || trim($data['title'], ' ') == ''){
        $data['title_err'] = 'Introduce un título';

      } else {
        $data['title'] = trim($data['title'], ' ');
      }
      
      if(!isset($data['body']) || trim($data['body'], ' ') == ''){
        $data['body_err'] = 'Añade contenido a tu publicación';

      } else {
        $data['body'] = trim($data['body'], ' ');
      }

      if (!empty($data['image'])) {
        try {
          $arrTypes = ["image/jpeg", "image/png", "image/gif"]; // Especifica la extensión de los archivos soportados para subirse
          $file = new File($_FILES, $arrTypes);
          
          $file->checkFileErrors();
          $file->saveUploadFile('img/');
  
        } catch (FileException $error) {
          $data['image_err'] = $error->getMessage();
        }
      } 

      if(isset($data['title_err']) || isset($data['body_err']) || isset($data['image_err']) || $data['image'] == ''){
        $this->view('posts/add', $data);

      } else {
        $data['user_id'] = $_SESSION['user_id'];

        if($this->post->addPost($data)){
          Flash::message('Mensaje subido.', 'success');
          
          urlHelper('home');
        
        } else {
          Flash::message('Algo salió mal y no se ha podido subir.', 'warning');
          
          urlHelper('home');
        }
      }

    } else {
      $this->view('posts/add');
    }
  }

  public function edit($id)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $args = [
        'title' => FILTER_SANITIZE_STRING,
        'body' => FILTER_SANITIZE_EMAIL,
      ];

      $data = filter_input_array(INPUT_POST, $args, true);

      if(!isset($data['title']) || trim($data['title'], ' ') == ''){
        $data['title_err'] = 'Introduce un título';

      } else {
        $data['title'] = trim($data['title'], ' ');
      }
      
      if(!isset($data['body']) || trim($data['body'], ' ') == ''){
        $data['body_err'] = 'Añade contenido a tu publicación';

      } else {
        $data['body'] = trim($data['body'], ' ');
      }

      if(isset($data['title_err']) || isset($data['body_err'])){
        $this->view('posts/add', $data);

      } else {
        $data['id'] = $id;

        if($this->post->updatePost($data)){
          Flash::message('Mensaje editado.', 'success');
          
          urlHelper('home');
        
        } else {
          Flash::message('Algo salió mal y no se ha podido editar.', 'warning');
          
          urlHelper('home');
        }
      }

    } else {
      $query = $this->post->getPostByID($id);

      $data['id'] = $id;
      $data['title'] = $query[0]->title;
      $data['body'] = $query[0]->body;

      if($query[0]->userId == $_SESSION['user_id']){
        $this->view('posts/edit', $data);
      
      } else {
        Flash::message('No tienes permiso para editar este mensaje.', 'warning');
          
        urlHelper('home');
      }
    }
  }

  public function delete($id)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $query = $this->post->getPostByID($id);

      if($query[0]->userId == $_SESSION['user_id']){
        if($this->post->deletePost($id)){
          Flash::message('Mensaje eliminado.', 'success');
          urlHelper('home');

        } else {
          Flash::message('Algo salió mal.', 'warning');
          urlHelper('home');
        }
      
      } else {
        Flash::message('No tiene permisos para eliminar ese post.', 'warning');
        urlHelper('home');
      }

    } else {
      Flash::message('No tiene permisos para realizar esa acción.', 'warning');
      urlHelper('home');
    }
  }
}
?>