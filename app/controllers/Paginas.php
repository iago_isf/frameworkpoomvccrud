<?php
class Paginas extends Controller
{
    
  public function __construct() 
  {
    
  }

  public function index()
  {
    if(isLoggedIn()){
      urlHelper('posts/index');
    } else {
      $this->view('paginas/index');
    }
  }

  public function about()
  {
    $this->view('paginas/about');
  }
}
?>