<?php
// Iniciar la sesion
if( !session_id() ) @session_start();
// Iniciar el autoarranque de composer
require_once 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->safeLoad();

require_once 'config/config.php';
require_once 'exceptions/FileException.php';
require_once 'helpers/utils.php';

spl_autoload_register(function ($className) {
  require_once 'libraries/'.$className.'.php';
});
?>