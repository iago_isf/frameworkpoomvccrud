<?php
class Post
{
  private $db;

  public function __construct()
  {
    $this->db = new Database();
  }

  public function getPosts()
  {
    $this->db->query('SELECT *,
      posts.id as postId,
      posts.created_at as postCreatedAt,
      users.id as userId ,
      users.created_at as userCreatedAt
      FROM posts INNER JOIN users
      ON posts.user_id = users.id
      ORDER BY posts.created_at DESC'
    );
  
    return $this->db->resultSet('Post');
  }

  public function addPost($data)
  {
    $this->db->query('INSERT INTO posts (title, body, user_id, image) VALUES (:title, :body, :user_id, :image)');
    $this->db->bind(':title', $data['title']);
    $this->db->bind(':body', $data['body']);
    $this->db->bind(':user_id', $data['user_id']);
    $this->db->bind(':image', $data['image']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  public function getPostById($id)
  {
    $this->db->query('SELECT
      posts.id as postId,
      posts.created_at as postCreatedAt,
      posts.title as title,
      posts.body as body,
      posts.image as image,
      users.id as userId,
      users.created_at as userCreatedAt,
      users.name as name
      FROM posts INNER JOIN users
      ON posts.user_id = users.id
      WHERE posts.id = :id'
    );
    
    $this->db->bind(':id', $id);

    return $this->db->resultSet('Post');
  }

  public function updatePost($data)
  {
    $this->db->query('UPDATE posts SET title = :title, body = :body WHERE id = :id');
    $this->db->bind(':title', $data['title']);
    $this->db->bind(':body', $data['body']);
    $this->db->bind(':id', $data['id']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  public function deletePost($id)
  {
    $this->db->query('DELETE FROM posts WHERE id = :id');
    $this->db->bind(':id', $id);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }
}
?>