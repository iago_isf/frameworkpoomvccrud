<?php
class User
{
  private $db;

  public function __construct()
  {
    $this->db = new Database();
  }

  public function findUserByEmail($email)
  {
    $this->db->query('SELECT * FROM users WHERE email = :email');
    $this->db->bind(':email', $email);

    $coincidences = $this->db->getRows();

    if($coincidences >= 1){
      return true;
    } else {
      return false;
    }
  }

  public function register($data)
  {
    $this->db->query('INSERT INTO users (name, email, password) VALUES (:name, :email, :password)');
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':email', $data['email']);
    $this->db->bind(':password', $data['password']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  public function login($data)
  {
    $this->db->query('SELECT * FROM users WHERE email=:email');
    $this->db->bind(':email', $data['email']);
    $logUser = $this->db->single('User');
    
    if(password_verify($data['password'], $logUser->password)){
      return $logUser;
    } else {
      return false;
    }
  }
}
?>