<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>

<div class="container">
  <div class="row">
    <div class="col">
      <div class="h-100 p-5 bg-light border rounded-3">
        <h2>Sobre nosotros</h2>
        <p>Or, keep it light and add a border for some added definition to the boundaries of your content. Be sure to look under the hood at the source HTML here as we've adjusted the alignment and sizing of both column's content for equal-height.</p>
        <p>Versión: <label class="btn btn-danger"><?= APPVERSION ?></label></p>
      </div>
    </div>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>