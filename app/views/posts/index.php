<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<div class="container">
  <div class="row mb-3">
    <div class="col-md-6">
      <h1>Publicaciones</h1>
    </div>
    <div class="col-md-6">
      <a class="btn btn-primary pull-right" href="<?= URLROOT ?>/posts/add" role="button">
        <i class="fas fa-pencil-alt"></i> Crear publicación
      </a>
    </div>
  </div>

  <div class="flashes">
    <?= (string) flash() ?>
  </div>

  <div class="row">
  <?php  
  foreach($data as $post) {
  ?>
    <div class="col col-12 mt-4">
      <div class="card">
        <div class="card-body">
          <div class="row mb-3">
            <?php if ($post->image) { ?>
            <div class="col col-3">
              <img src="<?= URLROOT . '/public/img/' . $post->image ?>" alt="Post image" class="w-100">
            </div>
            <?php } ?>
            <div class="col col-9">
              <h3 class="my-2"><?= $post->title ?></h3>
              <div class="my-2 text-muted fst-italic">Creado por <?= $post->name ?> el <?= $post->postCreatedAt?></div>
              <div class="my-2"><?= $post->body; ?></div>
            </div>
          </div>
          <a href="<?= URLROOT ?>/posts/show/<?= $post->postId ?>" class="btn btn-outline-warning w-100">Más</a>
        </div>
      </div>
    </div>
  <?php
  }
  ?>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>