<?php
include_once APPROOT . '/views/partials/header.php';
?>

<div class="container">
  <a class="btn btn-warning pull-right mt-3" href="<?= URLROOT ?>/posts/show/<?= $data['id'] ?>" role="button">
      <i class="fas fa-arrow-left"></i> Regresar
  </a>

  <div class="card card-body bg-light mt-3">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form action="<?= URLROOT ?>/posts/edit/<?= $data['id'] ?>" method="POST" class="needs-validation" novalidate>
      <div class="form-group">
        <label for="title">Título: <sup>*</sup></label>
        <input type="text" name="title" class="form-control <?= isset($data['title_err']) ? 'is-invalid' : 'is-valid'?>" placeholder="Título de la publicación" value="<?= $data['title'] ?>">
        <span class="invalid-feedback"><?= $data['title_err'] ? $data['title_err'] : ''?></span>
      </div>
      <div class="form-group">
        <label for="body">Contenido: <sup>*</sup></label>
        <textarea name="body" class="form-control <?= isset($data['body_err']) ? 'is-invalid' : 'is-valid'?>" rows="5" placeholder="Su contenido"><?= $data['body'] ?></textarea>

        <span class="invalid-feedback"><?= $data['body_err'] ? $data['body_err'] : ''?></span>
      </div>
      <div class="row">
        <div class="col">
          <input type="submit" value="Crear publicación" class="btn btn-primary btn-block mt-3">
        </div>
      </div>
    </form>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>