<?php
include_once APPROOT . '/views/partials/header.php';
?>

<div class="container">
  <a class="btn btn-warning pull-right mt-3" href="<?= URLROOT ?>/posts/index" role="button">
      <i class="fas fa-arrow-left"></i> Regresar
  </a>

  <div class="card card-body bg-light mt-3">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form action="<?= URLROOT ?>/posts/add" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
      <div class="form-group my-2">
        <label for="title">Título: <sup>*</sup></label>
        <input type="text" name="title" class="form-control <?= isset($data['title_err']) ? 'is-invalid' : ''?> <?= isset($data['title']) ? 'is-valid' : ''?>" placeholder="Título de la publicación" value="<?= isset($data['title']) ? $data['title'] : ''?>">
        <span class="invalid-feedback"><?= $data['title_err'] ? $data['title_err'] : ''?></span>
      </div>
      <div class="form-group my-2">
        <label for="body">Contenido: <sup>*</sup></label>
        <textarea name="body" class="form-control <?= isset($data['body_err']) ? 'is-invalid' : ''?> <?= isset($data['body'] ) ? 'is-valid' : ''?>" rows="5" placeholder="Su contenido"><?= isset($data['body']) ? $data['body'] : ''?></textarea>
        <span class="invalid-feedback"><?= $data['body_err'] ? $data['body_err'] : ''?></span>
      </div>
      <div class="form-group my-2">
        <label for="image">Imagen:</label>
        <input type="file" name="image" id="image" class="form-control <?= isset($data['image_err']) ? 'is-invalid' : ''?> <?= isset($data['image'] ) ? 'is-valid' : ''?>" >
        <span class="invalid-feedback"><?= $data['image_err'] ? $data['image_err'] : ''?></span>
      </div>

      <div class="row">
        <div class="col">
          <input type="submit" value="Crear publicación" class="btn btn-primary btn-block mt-3">
        </div>
      </div>
    </form>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>