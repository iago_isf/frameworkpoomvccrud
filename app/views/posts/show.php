<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>

<div class="container">
  <a class="btn btn-warning pull-right mt-3" href="<?= URLROOT ?>/home" role="button">
      <i class="fas fa-arrow-left"></i> Regresar
  </a>
  <br>
  <div class="row mb-3 mt-3">
    <div class="col-md-12">

      <h1><?= $data[0]->title ?></h1>

      <div class="bg-secondary text-white p-2 mb-3">
        Creado por: <?= $data[0]->name ?> el <?= $data[0]->postCreatedAt?>
      </div>
      <p>
          <?= $data[0]->body ?>
      </p>
      <?php if ($data[0]->image) { ?>
      <div class="text-center">
        <img src="<?= URLROOT . '/public/img/' . $data[0]->image ?>" alt="Post image" class="w-50">
      </div>
      <?php } ?>
      
      <hr>

      <div class="row">
        <div class="col col-6">
          <a href="<?= URLROOT ?>/posts/edit/<?= $data[0]->postId ?>" class="btn btn-success btn-block w-100">
            <i class="fas fa-edit"></i> Editar
          </a>
        </div>
        <div class="col col-6">
          <form action="<?= URLROOT ?>/posts/delete/<?= $data[0]->postId ?>" method="post">                        
            <button type="submit" class="btn btn-danger btn-block w-100">
              <i class="fas fa-trash"></i> Borrar post
            </button>
          </form>
        </div>
      </div>
        
    </div>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>