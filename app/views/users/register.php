<?php
include_once APPROOT . '/views/partials/header.php';
// include_once APPROOT . '/views/partials/navbar.php';
?>

<div class="row">
  <div class="col-md-6 mx-auto">
    <div class="card card-body bg-light mt-5">
      <h2> Crear una cuenta</h2>
      <p>Por favor llena los campos para poder registrarse</p>
      <form action="<?= URLROOT ?>/users/register" method="POST" class="needs-validation" novalidate>
        <div class="form-group">
          <label for="name">Nombre: <sup>*</sup></label>
          <input type="text" name="name" class="form-control <?php if(isset($data['name_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['name'])) echo $data['name'] ?>">
          <div class="invalid-feedback">
            Rellena el nombre.
          </div> 
        </div>
        <div class="form-group">
          <label for="email">Email: <sup>*</sup></label>
          <input type="email" name="email" class="form-control <?php if(isset($data['email_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['email'])) echo $data['email'] ?>">
          <div class="invalid-feedback">
            Rellena el email.
          </div>                    
        </div>
        <div class="form-group">
          <label for="password">Contraseña: <sup>*</sup></label>
          <input type="password" name="password" class="form-control <?php if(isset($data['password_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['password'])) echo $data['password'] ?>">
          <div class="invalid-feedback">
            Rellena la contraseña.
          </div> 
        </div>
        <div class="form-group">
          <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
          <input type="password" name="confirm_password" class="form-control <?php if(isset($data['confirm_password_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['confirm_password'])) echo $data['confirm_password'] ?>">
          <div class="invalid-feedback">
            Repite la contraseña.
          </div>                    
        </div>
        <div class="row">
          <div class="col">
            <a href="<?= URLROOT ?>/users/login">¿Ya tienes cuenta? Inicia sesión</a>
          </div>
          <div class="col">
            <input type="submit" value="Registrar" class="btn btn-primary btn-block">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>