<?php
include_once APPROOT . '/views/partials/header.php';
?>

<div class="flashes">
   <?= (string) flash() ?>
</div>

<div class="row">
  <div class="col-md-6 mx-auto">
    <div class="card card-body bg-light mt-5">
      <h2>Inicia sesion</h2>
      <p>Por favor llena los campos para poder registrarse</p>
      <form action="<?= URLROOT ?>/users/login" method="POST" class="needs-validation" novalidate>
        
        <div class="form-group">
          <label for="email">Email: <sup>*</sup></label>
          <input type="email" name="email" class="form-control <?php if(isset($data['email_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['email'])) echo $data['email'] ?>">
          <div class="invalid-feedback">
            Rellena el email.
          </div>                    
        </div>
        <div class="form-group">
          <label for="password">Contraseña: <sup>*</sup></label>
          <input type="password" name="password" class="form-control <?php if(isset($data['password_err'])) echo 'is-invalid' ?>" value="<?php if(isset($data['password'])) echo $data['password'] ?>">
          <div class="invalid-feedback">
            Rellena la contraseña.
          </div> 
        </div>
        <div class="row">
          <div class="col">
            <a href="<?= URLROOT ?>/users/register">¿No tienes cuenta? Créala ahora</a>
          </div>
          <div class="col">
            <input type="submit" value="Login" class="btn btn-primary btn-block">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>