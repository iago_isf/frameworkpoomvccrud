<div class="container">
  <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 border-bottom">
    <a href="<?php if(isLoggedIn()) echo URLROOT . '/posts/index' ; else URLROOT . '/paginas/index' ?>" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
      <i class="fas fa-home" style="font-size: 2rem;"></i>
    </a>

    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
      <li><a href="<?= URLROOT ?>/home" class="nav-link px-2 link-dark <?= isActive('home') ?>">Home</a></li>
      <li><a href="<?= URLROOT ?>/posts/index" class="nav-link px-2 link-dark <?= isActive('posts') ?>">Posts</a></li>
      <li><a href="<?= URLROOT ?>/paginas/about" class="nav-link px-2 link-dark <?= isActive('about') ?>">About</a></li>
    </ul>

    <div class="col-md-3 text-end">
    <?php if( isLoggedIn() ){ ?>
      <a href="<?= URLROOT ?>/users/logout" type="button" class="btn btn-primary">Logout</a>
    <?php } else { ?>
      <a href="<?= URLROOT ?>/users/login" type="button" class="btn btn-outline-primary me-2">Login</a>
      <a href="<?= URLROOT ?>/users/register" type="button" class="btn btn-primary">Sign-up</a>
    <?php }  ?>
    </div>
  </div>
</div>
<div class="container">
  <div class="col col-12 justify-content-center py-3 mb-4 border-bottom">
    <div class="text-center">
      <?php 
      if( isLoggedIn() ){ 
        echo 'Sesión iniciada como:' . $_SESSION['user_name']; 
        
      } else {
        echo '<a href="<?= URLROOT ?>/users/login" type="button" class="btn btn-outline-primary me-2">Inicie sesión</a>';
      } 
      ?>
      </div>
  </div>
</div>